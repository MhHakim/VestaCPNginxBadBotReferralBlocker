Disable following setting by adding <b>#</b> of <b>nginx.conf</b> in <b>http</b> section.<br/><code>#server_names_hash_max_size      512; <br/>#server_names_hash_bucket_size   512; </code>

Go <code>/etc/nginx/</code><br>Run <code>wget https://gitlab.com/MhHakim/VestaCPNginxBadBotReferralBlocker/raw/master/bots.d.zip && unzip bots.d.zip && rm bots.d.zip</code>

Go <code>/etc/nginx/conf.d</code><br>Run <code>wget https://gitlab.com/MhHakim/VestaCPNginxBadBotReferralBlocker/raw/master/conf.d.zip && unzip conf.d.zip && rm conf.d.zip</code>

Add the following at the bottom of websites nginx config file. Code must place before closing server section.
<code>
    include     /etc/nginx/bots.d/blockbots.conf;
    include     /etc/nginx/bots.d/ddos.conf;
</code>
